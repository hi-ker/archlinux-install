#!/bin/sh
printf "Installing paru"
git clone https://aur.archlinux.org/paru.git
cd paru
makepkg -si --noconfirm

printf "Downloading drivers.."
doas pacman -S --noconfirm xorg xf86-video-intel xf86-input-libinput nvidia nvidia-utils nvidia-settings

printf "Installing KDE"
doas pacman -S plasma sddm dolphin okular
doas pacman -S --asdeps dolphin-plugins vim-plugins
doas systemctl enable sddm.service

printf "Installing browser and multimedia packages"
doas pacman -S firefox chromium nvim elisa kdenlive youtube-dl mpv smplayer smplayer-skins smplayer-themes vlc papirus-icon-theme

printf "Installing virualization softwares"
doas pacman -S virt-manager qemu qemu-arch-extra edk2-ovmf bridge-utils dnsmasq vde2 openbsd-netcat iptables-nft ipset firewalld

printf "Reboot now."
